#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFile>
#include "qmessagebox.h"
#include "QTextStream"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void getSeperator();
    void getRegex();
    void getCharsInValue();
    QMessageBox msgBox;



public slots:
    void outputCsvFileDlg(void);
    void inputTextFileDlg(void);
    void convert(void);

private:
    QString inputTextAddress;
    QString outputCsvAddress;
    QFile outputCsvFile;
    QFile inputTextFile;
    QString seperator;
    QString regex;
    int charsInValue;
    QString rawData;
    QStringList processedData;
    QTextStream inputStream;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
