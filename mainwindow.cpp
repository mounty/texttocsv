#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qfiledialog.h"
#include "qmessagebox.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    this->connect(ui->browseOutputPushButton, SIGNAL(clicked()), this, SLOT(outputCsvFileDlg()));
    this->connect(ui->browseInputPushButton,SIGNAL(clicked()),this,SLOT(inputTextFileDlg()));
    this->connect(ui->convertPushButton,SIGNAL(clicked()),this,SLOT(convert()));
}
void MainWindow::outputCsvFileDlg(void)
{
    outputCsvAddress=QFileDialog::getSaveFileName(this, "Select Output File Name", "filename.csv", "CSV files (*.csv)", 0, 0);
    outputCsvFile.setFileName(outputCsvAddress);
    ui->selectOutputCsvFileLineEdit->setText(outputCsvAddress);

}

void MainWindow::inputTextFileDlg(void)
{
    inputTextAddress=QFileDialog::getOpenFileName(this, tr("Open Text File to convert"), "/home", tr("Text (*.txt)"));
    inputTextFile.setFileName(inputTextAddress);
    ui->selectInputTextFileLineEdit->setText(inputTextAddress);
}

void MainWindow::getSeperator()
{
    seperator=ui->seperatorLineEdit->text();

}

void MainWindow::getRegex()
{
    regex=ui->regexLineEdit->text();
}

void MainWindow::getCharsInValue()
{
    charsInValue=ui->valuesLength->value();
}

//////////////////////////////
////////// Real work /////////
//////////////////////////////
//////////////////////////////

void MainWindow::convert()
{
    this->statusBar()->showMessage("ready");
    ui->conversionProgressBar->setValue(0);
    getSeperator();

    getRegex();

    getCharsInValue();


    if (!(outputCsvAddress.isEmpty() || inputTextAddress.isEmpty() || seperator.isEmpty() ||regex.isEmpty()))
    {
         outputCsvFile.open( QIODevice::WriteOnly );
         inputTextFile.open(QIODevice::ReadOnly);

     }
    else
    {
        msgBox.setText(QString("please specify input/output file or seperator/regex"));
        msgBox.exec();
        return;
    }


    QRegExp rx(regex);
    rx.setPatternSyntax(QRegExp::Wildcard);
    QTextStream its(&inputTextFile);
    rawData=its.readAll();
    processedData=rawData.split(rx,QString::SkipEmptyParts);
    QTextStream ots(&outputCsvFile);
    QString temp;


    if (!processedData.isEmpty())
    {
         int it =0;
         while(it<processedData.size()){
             temp=processedData[it];
             int limit=temp.size();
             for (int count=0; count<limit; count=count+charsInValue)
             {
                 ots<<temp.left(charsInValue)<<seperator;
                 temp.remove(0,charsInValue);

             }
             ots<<'\n';




             ++it;
             ui->conversionProgressBar->setValue((int((it/processedData.size())*100)));
         }

    }
    else
    {
        msgBox.setText("Regex not have any index in file");
        msgBox.exec();
        return;
    }
    ots.flush();
    outputCsvFile.close();
    inputTextFile.close();
    this->statusBar()->showMessage("converted");


}

MainWindow::~MainWindow()
{
    delete ui;
}
